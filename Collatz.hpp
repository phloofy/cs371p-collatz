/* Collatz.hpp */

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

using beeg = unsigned long long;

/**
 * find the cycle length of a number
 * @param an int
 * @return an int
 */
int cycle_length(beeg);

/**
 * find the max cycle length between two numbers, inclusive
 * @param an int
 * @param an int
 * @return an int
 */
int max_cycle_length(int, int);

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
