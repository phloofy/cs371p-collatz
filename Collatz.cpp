/* Collatz.cpp */

// includes
#include <algorithm>	// max
#include <array>        // array
#include <cassert>	// assert
#include <iostream>	// endl, istream, ostream
#include <sstream>	// istringstream
#include <string>	// getline, string
#include <tuple>	// make_tuple, tie, tuple
#include <utility>	// make_pair, pair, swap
#include <vector>	// vector

#include "Collatz.hpp"

using namespace std;

static constexpr int MAX = 1000000;

using beeg = unsigned long long;

// find the cycle length of the given number
// precondtions: num > 0
// returns a positive integer
int cycle_length(beeg num) {

    assert(num > 0);

    // static variable is initialized once
    static array<int,(MAX >> 2)> cache{0};

    int len;

    // store intermediate values
    vector<pair<int,int>> map{};
    {
        int z = __builtin_ctzll(num);
        len = z+1;
        num = num >> z;
    }

    while (num != 1) {

        assert(num % 2 == 1);

        // check cache for num in range
        beeg k = num >> 1;
        if (k < cache.size()) {
            int v = cache[k];

            // already cached
            if (v != 0) {
                len +=v;
                break;
            }

            // cache later
            map.push_back(make_pair((int) k, len));
        }

        // find the next odd number in the sequence
        num = (num >> 1) + num + 1;
        int z = __builtin_ctzll(num);
        num = num >> z;
        len += z+2;
    }

    assert(len > 0);

    // cache intermediate values
    for (auto it = map.begin(); it != map.end(); ++it) {
        int k, d;
        tie(k, d) = *it;
        cache[k] = len-d;
    }

    return len;
}

// max_cycle_length
int max_cycle_length(int a, int b) {

    assert(a <= b);

    int m = 0;
    for (int i = a; i <= b; ++i)
        m = max(cycle_length((beeg) i), m);

    assert(m > 0);

    return m;
}

// collatz_read
pair<int, int> collatz_read (const string& s) {
    int i, j;
    // for gcov
    sscanf(s.c_str(), "%d %d", &i, &j);
    return make_pair(i, j);
}

// collatz_eval
tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    int a = min(i, j), b = max(i, j);
    return make_tuple(i, j, max_cycle_length(max(a, (b >> 1) + 1), b));
}

// collatz_print
void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// collatz_solve
void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
