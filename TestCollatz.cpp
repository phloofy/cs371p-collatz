// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

/* cycle_length tests */
TEST(CollatzFixture, cycle0) {
    ASSERT_EQ(cycle_length(1), 1);
}
TEST(CollatzFixture, cycle1) {
    ASSERT_EQ(cycle_length(3),8);
}
TEST(CollatzFixture, cycle2) {
    ASSERT_EQ(cycle_length(837799), 525);
}
TEST(CollatzFixture, cycle3) {
    ASSERT_EQ(cycle_length(524288), 20);
}

// integer overflow tests
TEST(CollatzFixture, cycle4) {
    ASSERT_EQ(cycle_length(270271), 407);
}
TEST(CollatzFixture, cycle5) {
    ASSERT_EQ(cycle_length(665215), 442);
}
TEST(CollatzFixture, cycle6) {
    ASSERT_EQ(cycle_length(704511), 243);
}

/* max_cycle_length tests */
TEST(CollatzFixture, maxcycle0) {
    ASSERT_EQ(max_cycle_length(1,10), 20);
}
TEST(CollatzFixture, maxcycle1) {
    ASSERT_EQ(max_cycle_length(100,200), 125);
}
TEST(CollatzFixture, maxcycle2) {
    ASSERT_EQ(max_cycle_length(201,210), 89);
}
TEST(CollatzFixture, maxcycle3) {
    ASSERT_EQ(max_cycle_length(900,1000), 174);
}

// 1-element ranges
TEST(CollatzFixture, maxcycle4) {
    ASSERT_EQ(max_cycle_length(10,10), 7);
}
TEST(CollatzFixture, maxcycle5) {
    ASSERT_EQ(max_cycle_length(1,1), 1);
}

/* eval tests */
TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}
TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}
TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}
TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

// reversed input tests
TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}
TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 900)), make_tuple(1000, 900, 174));
}
TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 1)), make_tuple(999999, 1, 525));
}
TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(524288,524287)), make_tuple(524288, 524287, 178));
}

// 1-element ranges
TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 999999)), make_tuple(999999, 999999, 259));
}
TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(999998, 999998)), make_tuple(999998, 999998, 259));
}

/* read & print tests */
TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}
TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

/* solve tests */
TEST(CollatzFixture, solve0) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
TEST(CollatzFixture, solve1) {
    istringstream sin("10 1\n200 100\n210 201\n1000 900\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n");
}
