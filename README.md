# CS371p: Object-Oriented Programming Collatz Repo

* Name: Ethan Tan

* EID: eyt249

* GitLab ID: phloofy

* HackerRank ID: phloofy

* Git SHA: 447e5751ae4849944f15a67cc763105ab248d599

* GitLab Pipelines: https://gitlab.com/phloofy/cs371p-collatz/-/pipelines

* Estimated completion time: 8 hours

* Actual completion time: 9 hours

* Comments: (any additional comments you have)

asserts are commented out in the code because they cause gcov to show decreased coverage

Generate acceptance tests with:
runghc Maketest <number of tests> > <output file>

Only works if you have ghc and the System.Random package (hackage)
Source in Maketest.hs