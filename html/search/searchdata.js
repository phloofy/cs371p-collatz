var indexSectionsWithContent =
{
  0: "bcmrt",
  1: "crt",
  2: "cmt",
  3: "m",
  4: "b",
  5: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "typedefs",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Typedefs",
  5: "Pages"
};

