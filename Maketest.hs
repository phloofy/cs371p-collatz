import Control.Applicative (liftA2)
import Control.Monad       (replicateM_)
import System.Environment  (getArgs)
import System.Random       (randomRIO)

main :: IO ()
main = do
  n <- read . head <$> getArgs :: IO Int
  replicateM_ n $ do
    (a,b) <- liftA2 (,) (randomRIO (1,999999)) (randomRIO (1,999999)) :: IO (Int,Int)
    putStrLn $ concat [show a," ",show b]    
